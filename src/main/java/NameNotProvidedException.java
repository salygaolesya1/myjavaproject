class NameNotProvidedException extends Exception {
    public NameNotProvidedException(String message) {
        super(message);
    }
}
