import java.util.*;

public class Lection10Maps {

    public static void main(String[] args) {
        String[] arr1 = {"a", "bb", "a", "bb"};
        String[] arr2 = {"this", "and", "that", "and"};
        String[] arr3 = {"code", "code", "code", "bug"};
        System.out.println(getLengthOfStrings(arr1));
        System.out.println(getLengthOfStrings(arr2));
        System.out.println(getLengthOfStrings(arr3));
        System.out.println();
        String[] string1 = {"code", "bug"};
        String[] string2 = {"man", "moon", "main"};
        String[] string3 = {"man", "moon", "good", "night"};
        System.out.println(createMap(string1));
        System.out.println(createMap(string2));
        System.out.println(createMap(string3));
        System.out.println();
        String[] string4 = {"a", "b", "a", "c", "b"};
        String[] string5 = {"c", "b", "a"};
        String[] string6 = {"c", "c", "c", "c"};
        System.out.println(countWords(string4));
        System.out.println(countWords(string5));
        System.out.println(countWords(string6));
        System.out.println();
        String[] test1 = {"salt", "tea", "soda", "toast"};
        String[] test2 = {"aa", "bb", "cc", "aAA", "cCC", "d"};
        String[] test3 = {};
        System.out.println(mergeStringsByFirstChar(test1));
        System.out.println(mergeStringsByFirstChar(test2));
        System.out.println(mergeStringsByFirstChar(test3));
        System.out.println();
        String[] test4 = {"a", "b", "a"};
        String[] test5 = {"a", "b", "a", "c", "a", "d", "a"};
        String[] test6 = {"a", "", "a"};
        System.out.println(buildResultString(test4));
        System.out.println(buildResultString(test5));
        System.out.println(buildResultString(test6));
        System.out.println();
        int[] nums = {3, 1, 2, 2, 1, 2, 3, 3, 3};
        System.out.println("Most frequent element: " + findMostFrequentElement(nums));
        System.out.println();
        LinkedHashSet<Integer> setA = new LinkedHashSet<>();
        LinkedHashSet<Integer> setB = new LinkedHashSet<>();

        setA.add(1);
        setA.add(2);
        setA.add(3);
        setA.add(4);

        setB.add(3);
        setB.add(5);
        setB.add(6);

        boolean result = hasIntersection(setA, setB);
        System.out.println("Sets have intersection: " + result);
        System.out.println();
        TreeSet<Integer> set1 = new TreeSet<>();
        TreeSet<Integer> set2 = new TreeSet<>();

        setA.add(1);
        setA.add(2);
        setA.add(3);

        setB.add(3);
        setB.add(4);
        setB.add(5);

        TreeSet<Integer> setC = mergeTreeSets(set1, set2);

        System.out.println("Merged TreeSet C: " + setC);

    }

    public static Map<String, Integer> getLengthOfStrings(String[] arr) {
        Map<String, Integer> resultMap = new HashMap<>();

        for (String str : arr) {
            if (!resultMap.containsKey(str)) {
                resultMap.put(str, str.length());
            }

        }
        return resultMap;

    }


    public static Map<String, String> createMap(String[] string) {
        Map<String, String> result = new HashMap<>();

        for (String str : string) {
            if (!str.isEmpty()) {
                char firstChar = str.charAt(0);
                char lastChar = str.charAt(str.length() - 1);
                if (!result.containsKey(String.valueOf(firstChar))) {
                    result.put(String.valueOf(firstChar), String.valueOf(lastChar));
                }

            }
        }
        return result;

    }

    public static Map<String, Integer> countWords(String[] arr) {
        Map<String, Integer> result = new HashMap<>();

        for (String str : arr) {
            if (result.containsKey(str)) {
                result.put(str, result.get(str) + 1);
            } else {
                result.put(str, 1);
            }
        }

        return result;
    }

    public static Map<String, String> mergeStringsByFirstChar(String[] strings) {
        Map<String, String> resultMap = new HashMap<>();

        for (String str : strings) {
            String firstChar = str.substring(0, 1);

            if (resultMap.containsKey(firstChar)) {
                String updatedValue = resultMap.get(firstChar) + str;
                resultMap.put(firstChar, updatedValue);
            } else {
                resultMap.put(firstChar, str);
            }
        }
        return resultMap;

    }

    public static String buildResultString(String[] strings) {
        Set<String> seen = new HashSet<>();
        StringBuilder result = new StringBuilder();

        for (String str : strings) {
            if (!seen.contains(str)) {
                seen.add(str);
                result.append(str);
            }
        }

        return result.toString();
    }

    public static int findMostFrequentElement(int[] nums) {
        Map<Integer, Integer> countMap = new HashMap<>();
        int maxCount = 0;
        int mostFrequentElement = nums[0];

        for (int num : nums) {
            if (countMap.containsKey(num)) {
                countMap.put(num, countMap.get(num) + 1);
            } else {
                countMap.put(num, 1);
            }

            if (countMap.get(num) > maxCount) {
                maxCount = countMap.get(num);
                mostFrequentElement = num;
            }
        }

        return mostFrequentElement;
    }


    public static boolean hasIntersection(LinkedHashSet<Integer> setA, LinkedHashSet<Integer> setB) {
        LinkedHashSet<Integer> intersection = new LinkedHashSet<>(setA);

        intersection.retainAll(setB);

        return !intersection.isEmpty();
    }

    public static TreeSet<Integer> mergeTreeSets(TreeSet<Integer> setA, TreeSet<Integer> setB) {
        TreeSet<Integer> setC = new TreeSet<>(setA);

        setC.addAll(setB);

        return setC;
    }
}







