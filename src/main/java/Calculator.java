public class Calculator {
    public static void main(String[] args) {
        int a = 5;
        square(a);
        double radius = 3.5;
        double height = 10.0;
        calculateVolume(radius, height);
        int value =2;
        int powValue =3;
        pow(value, powValue);
    }

    public static void square(int a) {
        int result = a * a;
        System.out.println("Квадрат числа " + a + " дорівнює " + result);
    }

    public static double calculateVolume (double radius, double height){
        double volume = Math.PI*radius*radius*height;
        System.out.println("Об'єм циліндра з радіусом " + radius + " і висотою " + height + " дорівнює " + volume);
        return volume;
    }

    public static int pow(int value, int powValue) {
        int powResult = (int) Math.pow(value,powValue);
        System.out.println( value + " приведене до степеня " + powValue + " дорівнює " + powResult);
        return powResult;

    }


}




