public class Lection6Strings {

    public static void main(String[] args) {
        System.out.println(repeatEnd("Hello", 3));
        System.out.println(repeatEnd("Hello", 2));
        System.out.println(repeatEnd("Hello", 1));

        System.out.println(mixString("abc", "xyz"));
        System.out.println(mixString("Hi", "There"));
        System.out.println(mixString("xxxx", "There"));

        System.out.println(xyzMiddle("AAxyzBB"));
        System.out.println(xyzMiddle("AxyzBB"));
        System.out.println(xyzMiddle("AxyzBBB"));

        System.out.println(zipZap("zipXzap"));
        System.out.println(zipZap("zpzp"));
        System.out.println(zipZap("zzzpzp"));

        System.out.println(xyzThere("abcxyz"));
        System.out.println(xyzThere("abc.xyz"));
        System.out.println(xyzThere("xyz.abc"));


    }

    public static String repeatEnd(String str, int n) {
        String end = str.substring(str.length() - n);
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < n; i++) {
            result.append(end);
        }
        return result.toString();
    }

    public static String mixString(String a, String b) {
        StringBuilder result = new StringBuilder();
        int minLength = Math.min(a.length(), b.length());
        for (int i = 0; i < minLength; i++) {
            result.append(a.charAt(i));
            result.append(b.charAt(i));
        }
        if (a.length() > b.length()) {
            result.append(a.substring(minLength));
        } else if (b.length() > a.length()) {
            result.append(b.substring(minLength));
        }
        return result.toString();
    }

    public static boolean xyzMiddle(String str) {
        int len = str.length();
        if (len < 3) return false;
        int mid = len / 2;
        if (len % 2 == 0) {
            return (str.substring(mid - 2, mid + 1).equals("xyz") ||

                    str.substring(mid - 1, mid + 2).equals("xyz"));
        } else {
            return (str.substring(mid - 1, mid + 2).equals("xyz"));
        }


    }

    public static String zipZap(String str) {
        StringBuilder result = new StringBuilder();
        int i = 0;
        int len = str.length();

        while (i < len - 2) {
            if (i <= len - 3 && str.charAt(i) == 'z' && str.charAt(i + 2) == 'p') {
                result.append("zp");
                i += 3;
            } else {
                result.append(str.charAt(i));
                i++;
            }
        }

        while (i < len) {
            result.append(str.charAt(i));
            i++;
        }

        return result.toString();
    }

    public static boolean xyzThere(String str) {
        int len = str.length();

        for (int i = 0; i < len - 2; i++) {
            if (str.charAt(i) == 'x' && str.startsWith("xyz", i)) {
                if (i > 0 && str.charAt(i - 1) == '.') {
                    continue;
                } else {
                    return true;
                }
            }
        }

        return false;
    }

}

