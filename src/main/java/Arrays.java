import java.util.Scanner;

public class Arrays {

    public static void main(String[] args) {

        int[] numbers = {10, 20, 30, 40, 50, 60, 65, 70, 75};
        System.out.println("Average of the numbers : " + averageValue(numbers));

        System.out.println("Minimal value of numbers: " + calculateMinValue(numbers));
        System.out.println("Maximum value of numbers: " + calculateMaxValue(numbers));
        printReverse(numbers);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number to search in the array:");
        int number = scanner.nextInt();

        boolean found = numberInArray(numbers, number);


        if (found) {
            System.out.println(number + " is present in the array.");
        } else {
            System.out.println(number + " is  not present in the array.");

        }
    }

    public static double averageValue(int[] array) {
        if (array.length == 0) {
            return 0.0;
        }

        int sum = 0;
        for (int num : array) {
            sum += num;
        }

        return (double) sum / array.length;
    }

    public static int calculateMinValue(int[] array) {
        int min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (min > array[i]) {
                min = array[i];
            }

        }
        return min;

    }

    public static int calculateMaxValue(int[] array) {
        int max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (max < array[i]) {
                max = array[i];
            }

        }
        return max;

    }

    public static void printReverse(int[] array) {
        if (array == null || array.length == 0) {
            System.out.println("Array is empty or null");
            return;
        }

        System.out.print("Array in reverse order: ");
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static boolean numberInArray(int[] array, int number) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                return true;
            }
        }
        return false;
    }

}

       





