import java.util.Scanner;

public class Lection5 {

    public static void main(String[] args) {
        System.out.println(checkAge(18));
        System.out.println(checkAge(0));
        System.out.println(checkAge(99));

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите целое число");
        int num = scanner.nextInt();
        boolean result = isPerfectSquare(num);
        System.out.println("isPerfectSquare(" + num + ") " + result);

        Scanner scanner1 = new Scanner(System.in);
        System.out.println("Введите число ");
        int N = scanner1.nextInt();
        int sum = calculateSumUpToN(N);
        System.out.println("Сума чисел від 1 до " + N + " = " + sum);

        Scanner scanner2 = new Scanner(System.in);
        System.out.println("Введите оценку от 1 до 5");
        int grade = scanner2.nextInt();
        String resultOfGrade = getGrade(grade);
        System.out.println(resultOfGrade);

        Scanner scanner3 = new Scanner(System.in);
        System.out.print("Введіть число Q: ");
        int Q = scanner3.nextInt();
        boolean isPrime = isPrime(Q);
        if (isPrime) {
            System.out.println(Q + " є простим числом.");
        } else {
            System.out.println(Q + " не є простим числом.");
        }
    }


    public static String checkAge(int age) {
        if (age >= 18) {
            return "Ви доросла особа";
        } else {
            return "Ви не є дорослою особою";
        }
    }


    public static boolean isPerfectSquare(int num) {
        if (num < 0) {
            return false;
        }
        int sqr = (int) Math.sqrt(num);
        return sqr * sqr == num;


    }

    public static int calculateSumUpToN(int N) {
        int sum = 0;
        for (int i = 1; i <= N; i++) {
            sum += i;
        }
        return sum;
    }

    public static String getGrade(int grade) {
        switch (grade) {
            case 1:
                return "Погано";

            case 2:
                return "Погано";
            case 3:
                return "Добре";
            case 4:
                return "Добре";
            case 5:
                return "Відмінно";
            default:
                return "Неправильна оцінка";

        }

    }

    public static boolean isPrime(int number) {
        if (number <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }

}







