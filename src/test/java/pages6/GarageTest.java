import io.qameta.allure.*;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.asserts.SoftAssert;

public class GarageTest {
    private WebDriver driver;
    private GaragePage garagePage;
    private SoftAssert softAssert;

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "/Users/mac/IdeaProjects/MyJavaProject/src/main/resources/chromedriver");
        driver = new ChromeDriver();
        softAssert = new SoftAssert();
    }

    @Test
    @Description("Тест перевірки додавання авто до гаража")
    @Owner("Ваше ім'я")
    @Link("http://link-to-your-task")
    @Severity(SeverityLevel.NORMAL)
    public void testAddCar() {
        driver.get("https://guest:welcome2qauto@qauto.forstudy.space/");
        driver.findElement(By.id("guestLoginButton")).click();

        Assertions.assertEquals("https://qauto.forstudy.space/panel/garage", driver.getCurrentUrl());

        garagePage = new GaragePage(driver);
        garagePage.addCar("Audi", "TT", 20);

        softAssert.assertTrue(garagePage.isCarDisplayed("Audi", "TT"), "Автомобіль не відображається на сторінці Garage.");
        softAssert.assertEquals("середина вересня 2024", garagePage.getCurrentDate(), "Дата в 'car_update-mileage' не відповідає.");
        softAssert.assertEquals(20, garagePage.getMileage(), "Значення в інпуті 'miles' не відповідає 20.");
        softAssert.assertTrue(garagePage.isCarLogoDisplayed(), "Зображення автомобіля не відображається або невірний URL.");

        softAssert.assertAll();
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }
}
