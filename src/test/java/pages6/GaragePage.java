package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GaragePage {
    private WebDriver driver;

    public GaragePage(WebDriver driver) {
        this.driver = driver;
    }

    public void addCar(String brand, String model, int mileage) {
        driver.findElement(By.id("addCarButton")).click();
        driver.findElement(By.id("brandInput")).sendKeys(brand);
        driver.findElement(By.id("modelInput")).sendKeys(model);
        driver.findElement(By.id("mileageInput")).sendKeys(String.valueOf(mileage));
        driver.findElement(By.id("addButton")).click();
    }

    public boolean isCarDisplayed(String brand, String model) {
        String carInfo = driver.findElement(By.xpath("//div[contains(@class, 'car-info')]")).getText();
        return carInfo.contains(brand) && carInfo.contains(model);
    }

    public String getCurrentDate() {
        return driver.findElement(By.id("car_update-mileage")).getText();
    }

    public int getMileage() {
        return Integer.parseInt(driver.findElement(By.id("miles")).getAttribute("value"));
    }

    public boolean isCarLogoDisplayed() {
        WebElement logo = driver.findElement(By.className("car_logo car-logo"));
        return logo.isDisplayed() && logo.getAttribute("src").endsWith("audi.png");
    }
}

