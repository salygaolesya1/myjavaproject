import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class WebElementTests {

    public static void main(String[] args) {
        System.setProperty("chromedriver", "/Users/mac/IdeaProjects/MyJavaProject/src/main/resources/chromedriver");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");

        WebDriver driver = new ChromeDriver(options);

        try {
            driver.get("https://guest:welcome2qauto@qauto.forstudy.space/");
            checkLogoDisplay(driver);
            checkSignUpButtonColor(driver);

        } catch (Exception e) {
            System.out.println("An error occurred: " + e.getMessage());
        } finally {
            driver.quit();
        }
    }

    public static void checkLogoDisplay(WebDriver driver) {
        try {
            WebElement logo = driver.findElement(By.id("logo"));
            if (logo.isDisplayed()) {
                System.out.println("Logo displayed");
            } else {
                System.out.println("Logo does not displayed");
            }
        } catch (Exception e) {
            System.out.println("Logo does not displayed");
        }
    }

    public static void checkSignUpButtonColor(WebDriver driver) {
        try {
            WebElement signUpButton = driver.findElement(By.xpath("//button[text()='Sign up']"));
            String backgroundColor = signUpButton.getCssValue("background-color");

            if (backgroundColor.equals("rgba(2, 117, 216, 1)")) {
                System.out.println("Background color of Sign up button is correct");
            } else {
                System.out.println("Background color of Sign up button is incorrect");
            }
        } catch (Exception e) {
            System.out.println("Failed to retrieve background color");
        }
    }
}
