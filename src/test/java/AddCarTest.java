import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pages3.LoginPage;
import pages3.GaragePage;

import java.time.LocalDate;

public class AddCarTest {
    private WebDriver driver;
    private final String baseUrl = "https://guest:welcome2qauto@qauto.forstudy.space/";

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "path/to/chromedriver");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-notifications");
        driver = new ChromeDriver(options);
    }

    @Test
    @Description("Test for adding a car to the garage and verifying its presence.")
    @Owner("Salyha")
    @Severity(SeverityLevel.CRITICAL)
    public void testAddCarToGarage() {
        driver.get(baseUrl);

        LoginPage loginPage = new LoginPage(driver);
        loginPage.clickLoginButton();

        GaragePage garagePage = new GaragePage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), "https://qauto.forstudy.space/panel/garage");

        garagePage.clickAddCarButton();
        garagePage.addCar("Audi", "TT", "20");

        SoftAssert softAssert = new SoftAssert();

        softAssert.assertTrue(garagePage.isCarInList("Audi TT"), "Car not found in the list.");
        softAssert.assertEquals(garagePage.getMileageText(), LocalDate.now().toString(), "Mileage date is incorrect.");
        softAssert.assertEquals(garagePage.getMileageInputValue(), "20", "Mileage value is incorrect.");
        softAssert.assertTrue(garagePage.isCarLogoImageDisplayed(), "Car logo image is not displayed.");
        softAssert.assertTrue(garagePage.getCarLogoImageSrc().endsWith("audi.png"), "Car logo image src is incorrect.");

        softAssert.assertAll();
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
