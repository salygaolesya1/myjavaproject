import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.Color;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WebTests {
    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
        driver = new ChromeDriver();
    }

    @Test
    public void testLogoDisplay() {
        driver.get("https://guest:welcome2qauto@qauto.forstudy.space/");

        try {
            WebElement logo = driver.findElement(By.cssSelector("svg[width=\"135\"][height=\"30\"]"));

            Assert.assertTrue(logo.isDisplayed(), "Logo does not displayed");
        } catch (Exception e) {
            Assert.fail("Logo does not displayed");
        }
    }

    @Test
    public void testSignUpButtonColor() {
        driver.get("https://guest:welcome2qauto@qauto.forstudy.space/");

        try {
            WebElement signUpButton = driver.findElement(By.cssSelector("button.btn.btn-outline-white.header_signin\n"));

            String backgroundColor = signUpButton.getCssValue("background-color");

            String expectedColorHex = "#0275d8";
            String actualColorHex = Color.fromString(backgroundColor).asHex();

            Assert.assertEquals(actualColorHex, expectedColorHex, "Background color of Sign up button is incorrect");
        } catch (Exception e) {
            Assert.fail("Sign up button not found or background color could not be retrieved");
        }
    }

    @AfterClass
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}

