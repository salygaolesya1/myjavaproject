
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pages2.LoginPage;
import pages2.GaragePage;
import pages2.InstructionPage;

import java.io.File;

import static org.junit.Assert.assertTrue;

public class QAutoTests {
    private WebDriver driver;
    private final String baseUrl = "https://guest:welcome2qauto@qauto.forstudy.space/";

    @Before
    public void setUp() {
        System.setProperty("chromedriver", "/Users/mac/IdeaProjects/MyJavaProject/src/main/resources/chromedriver");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-notifications");
        driver = new ChromeDriver(options);
    }

    @Test
    public void testFileDownload() {
        driver.get(baseUrl);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.clickLoginButton();

        String expectedUrl = "https://qauto.forstudy.space/panel/garage";
        assertTrue(driver.getCurrentUrl().equals(expectedUrl));

        GaragePage garagePage = new GaragePage(driver);
        InstructionPage instructionPage = garagePage.goToInstructions();
        instructionPage.downloadFile();

        File downloadedFile = new File("Front windshield wipers on Audi TT.pdf");
        assertTrue(downloadedFile.exists());
    }

    @Test
    public void testCarListToTxtFile() {
        driver.get(baseUrl);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.clickGuestLogin();

        String expectedUrl = "https://qauto.forstudy.space/panel/garage";
        assertTrue(driver.getCurrentUrl().equals(expectedUrl));

        GaragePage garagePage = new GaragePage(driver);
        InstructionPage instructionsPage = garagePage.goToInstructions();
        instructionsPage.selectCarAndSaveList();

        File file = new File("cars.txt");
        assertTrue(file.exists());
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}

