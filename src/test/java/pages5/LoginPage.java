package pages5;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;

public class LoginPage {
    private SelenideElement emailInput = $x("//input[@name='email']");
    private SelenideElement passwordInput = $x("//input[@name='password']");
    private SelenideElement signInButton = $x("//button[text()='Sign In']");
    private SelenideElement errorMessage = $x("//div[@class='error-message']");

    public LoginPage open(String url) {
        open("https://guest:welcome2qauto@qauto.forstudy.space/");
        return this;
    }

    public LoginPage enterCredentials(String email, String password) {
        emailInput.setValue(email);
        passwordInput.setValue(password);
        return this;
    }

    public LoginPage submit() {
        signInButton.click();
        return this;
    }

    public LoginPage verifyErrorMessage(String expectedMessage) {
        errorMessage.shouldHave(text(expectedMessage));
        return this;
    }
}

