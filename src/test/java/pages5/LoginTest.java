package pages5;

import com.codeborne.selenide.Configuration;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.api.BeforeAll;

public class LoginTest {

    @BeforeAll
    static void setUp() {
        Configuration.browser = "chrome";
    }

    @ParameterizedTest
    @CsvSource({
            "test@hillel.ua, 1111",
            "test@hillel.ua, 1234"
    })
    void testInvalidLogin(String email, String password) {
        new LoginPage()
                .open("https://guest:welcome2qauto@qauto.forstudy.space/")
                .enterCredentials(email, password)
                .submit()
                .verifyErrorMessage("Wrong email or password");
    }
}

