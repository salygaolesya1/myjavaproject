import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;

public class AuthenticationTest {

    private static final String CHROME_DRIVER_PATH = "/Users/mac/IdeaProjects/MyJavaProject/src/main/resources/chromedriver";
    private static final String FIREFOX_DRIVER_PATH = "/Users/mac/IdeaProjects/MyJavaProject/src/main/resources/geckodriver-v0.35.0-macos.tar.gz";

    private static final String TEST_URL = "https://guest:welcome2qauto@qauto.forstudy.space/";
    private static final String EMAIL = "test@hillel.ua";
    private static final String PASSWORD = "1111";
    private static final String EXPECTED_ERROR_MESSAGE = "Wrong email or password";

    public static void main(String[] args) {
        try {
            testInChrome();
            testInFirefox();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void testInChrome() throws Exception {
        System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_PATH);
        WebDriver driver = new ChromeDriver();
        runTest(driver);
        driver.quit();
    }

    private static void testInFirefox() throws Exception {
        System.setProperty("webdriver.gecko.driver", FIREFOX_DRIVER_PATH);
        WebDriver driver = new FirefoxDriver();
        runTest(driver);
        driver.quit();
    }

    private static void runTest(WebDriver driver) throws Exception {
        driver.get(TEST_URL);

        WebElement signInButton = driver.findElement(By.cssSelector("button.sign-in"));
        signInButton.click();

        WebElement emailInput = driver.findElement(By.id("email"));
        WebElement passwordInput = driver.findElement(By.id("password"));
        emailInput.sendKeys(EMAIL);
        passwordInput.sendKeys(PASSWORD);

        WebElement loginButton = driver.findElement(By.cssSelector("button.login"));
        loginButton.click();

        WebElement errorMessageElement = driver.findElement(By.cssSelector(".error-message"));
        String actualErrorMessage = errorMessageElement.getText();

        if (EXPECTED_ERROR_MESSAGE.equals(actualErrorMessage)) {
            System.out.println("Test passed: Error message is correctly displayed.");
        } else {
            System.out.println("Test failed: Error message is not as expected.");
        }
    }
}

