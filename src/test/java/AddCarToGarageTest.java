
import pages.AddCarPopup;
import pages.GaragePage;
import pages.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AddCarToGarageTest extends BaseTest {
    private SoftAssert softAssert = new SoftAssert();

    @Test
    public void testAddCarToGarage() {
        driver.get("https://guest:welcome2qauto@qauto.forstudy.space/");

        LoginPage loginPage = new LoginPage(driver);
        loginPage.clickGuestLogin();

        String expectedUrl = "https://qauto.forstudy.space/panel/garage";
        String actualUrl = driver.getCurrentUrl();
        softAssert.assertEquals(actualUrl, expectedUrl, "Failed to navigate to Garage page.");

        GaragePage garagePage = new GaragePage(driver);
        garagePage.clickAddCar();

        AddCarPopup addCarPopup = new AddCarPopup(driver);
        addCarPopup.setCarDetails("Audi", "TT", "20");
        addCarPopup.clickAdd();

        softAssert.assertTrue(garagePage.isCarDisplayed("Audi TT"), "Car 'Audi TT' is not displayed on the Garage page.");

        String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        softAssert.assertTrue(garagePage.getMileageText().contains(currentDate), "Current date is not displayed in 'car_update-mileage'.");

        softAssert.assertEquals(garagePage.getMilesValue(), "20", "Mileage value is not '20'.");

        softAssert.assertTrue(garagePage.isCarLogoDisplayed(), "Car logo image is not displayed.");
        softAssert.assertTrue(garagePage.getCarLogoSrc().endsWith("audi.png"), "Image source does not end with 'audi.png'.");

        softAssert.assertAll();
    }
}

