import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class WebAutomationTest {

    public static void main(String[] args) {
        System.setProperty("chromedriver", "/Users/mac/IdeaProjects/MyJavaProject/src/main/resources/chromedriver");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");

        WebDriver driver = new ChromeDriver(options);

        try {
            driver.get("https://guest:welcome2qauto@qauto.forstudy.space/");

            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            wait.until(ExpectedConditions.titleIs("Hillel Qauto"));
            System.out.println("Page title is as expected: 'Hillel Qauto'");

            WebElement signInButton = driver.findElement(By.xpath("//button[text()='Sign In']"));
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].click();", signInButton);
            System.out.println("Clicked on 'Sign In' button");

            WebElement addCarButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Add car']")));
            System.out.println("Button 'Add car' is clickable");

        } catch (Exception e) {
            System.out.println("An error occurred: " + e.getMessage());
        } finally {
            driver.quit();
        }
    }
}
