import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class FirstTestSelenium {

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("chromedriver", "/Users/mac/IdeaProjects/MyJavaProject/src/main/resources/chromedriver");
        WebDriver driver = new ChromeDriver();

        try {
            driver.get("https://guest:welcome2qauto@qauto.forstudy.space/");
            driver.manage().window().maximize();
            WebElement element = driver.findElement(By.xpath("//*[contains(@class ,'header-link -guest')]"));
            element.click();
            WebElement menuItems = driver.findElement(By.cssSelector(".col-3"));
            System.out.println(menuItems.getText());

            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            driver.quit();

        }

    }
}
