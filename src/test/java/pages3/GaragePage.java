package pages3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GaragePage {
    private WebDriver driver;
    private By addCarButton = By.id("add-car-button"); // Adjust selector
    private By carList = By.cssSelector(".car-list"); // Adjust selector
    private By mileageParagraph = By.id("car_update-mileage"); // Adjust selector
    private By mileageInput = By.id("miles"); // Adjust selector
    private By carLogo = By.cssSelector(".car_logo.car-logo img"); // Adjust selector
    private By carLogoSrc = By.cssSelector(".car_logo.car-logo img[src]"); // Adjust selector

    public GaragePage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickAddCarButton() {
        driver.findElement(addCarButton).click();
    }

    public void addCar(String brand, String model, String mileage) {
        driver.findElement(By.id("brand-input")).sendKeys(brand); // Adjust selector
        driver.findElement(By.id("model-input")).sendKeys(model); // Adjust selector
        driver.findElement(By.id("mileage-input")).sendKeys(mileage); // Adjust selector
        driver.findElement(By.id("add-button")).click(); // Adjust selector
    }

    public boolean isCarInList(String car) {
        WebElement carListElement = driver.findElement(carList);
        return carListElement.getText().contains(car);
    }

    public String getMileageText() {
        return driver.findElement(mileageParagraph).getText();
    }

    public String getMileageInputValue() {
        return driver.findElement(mileageInput).getAttribute("value");
    }

    public boolean isCarLogoImageDisplayed() {
        return driver.findElement(carLogo).isDisplayed();
    }

    public String getCarLogoImageSrc() {
        return driver.findElement(carLogoSrc).getAttribute("src");
    }
}

