package pages2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class GaragePage {
    private WebDriver driver;

    private final By instructionLink = By.linkText("Instructions");

    public GaragePage(WebDriver driver) {
        this.driver = driver;
    }

    public InstructionPage goToInstructions() {
        driver.findElement(instructionLink).click();
        return new InstructionPage(driver);
    }
}

