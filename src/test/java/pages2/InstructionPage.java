package pages2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class InstructionPage {
    private WebDriver driver;

    private By fileLink = By.linkText("Front windshield wipers on Audi TT.pdf");
    private By carSelectButton = By.id("car-select-button"); // Adjust this selector
    private By carItems = By.cssSelector(".car-item"); // Adjust this selector

    public InstructionPage(WebDriver driver) {
        this.driver = driver;
    }

    public void downloadFile() {
        WebElement downloadElement = driver.findElement(fileLink);
        downloadElement.click();
    }

    public void selectCarAndSaveList() {
        driver.findElement(carSelectButton).click();
        List<WebElement> carElements = driver.findElements(carItems);

        try (FileWriter writer = new FileWriter(new File("cars.txt"))) {
            for (WebElement car : carElements) {
                writer.write(car.getText() + System.lineSeparator());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

