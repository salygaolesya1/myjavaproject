import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CalculatorTest {

    private Calculator_2 calculator;

    @BeforeClass
    public void setUp() {
        calculator = new Calculator_2();
    }

    @AfterClass
    public void tearDown() {
        calculator = null;
    }

    @Test(description = "Test addition operation", priority = 1)
    public void testAddition() {
        int result = calculator.add(5, 3);
        System.out.println("Addition Result: " + result);
        Assert.assertEquals(result, 8, "Addition result is incorrect");
    }

    @Test(description = "Test subtraction operation", priority = 2)
    public void testSubtraction() {
        int result = calculator.subtract(5, 3);
        System.out.println("Subtraction Result: " + result);
        Assert.assertEquals(result, 2, "Subtraction result is incorrect");
    }

    @Test(description = "Test multiplication operation", priority = 3)
    public void testMultiplication() {
        int result = calculator.multiply(5, 3);
        System.out.println("Multiplication Result: " + result);
        Assert.assertEquals(result, 15, "Multiplication result is incorrect");
    }

    @Test(description = "Test division operation", priority = 4)
    public void testDivision() {
        int result = calculator.divide(6, 3);
        System.out.println("Division Result: " + result);
        Assert.assertEquals(result, 2, "Division result is incorrect");
    }

    @Test(description = "Test division by zero", expectedExceptions = ArithmeticException.class, priority = 5)
    public void testDivisionByZero() {
        calculator.divide(1, 0);
    }
}

