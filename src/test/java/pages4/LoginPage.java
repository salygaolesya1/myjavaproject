package pages4;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.*;

public class LoginPage {
    private SelenideElement guestLoginButton = $x("//button[text()='Guest log in']");

    public GaragePage loginAsGuest() {
        guestLoginButton.click();
        return new GaragePage();
    }
}
