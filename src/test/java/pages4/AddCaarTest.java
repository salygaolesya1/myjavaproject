package pages4;

import com.codeborne.selenide.Configuration;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;

import static jdk.internal.misc.ThreadFlock.open;

public class AddCarTest {

    @BeforeAll
    static void setUp() {
        Configuration.browser = "chrome";
        Configuration.baseUrl = "https://qauto.forstudy.space";
    }

    @Test
    void testAddCarAsGuest() {
        open("https://guest:welcome2qauto@qauto.forstudy.space/");
        LoginPage loginPage = new LoginPage();

        GaragePage garagePage = loginPage.loginAsGuest();

        AddCarModal addCarModal = garagePage.openAddCarModal();

        garagePage = addCarModal.addCar("Audi", "TT", "20");

        String todayDate = LocalDate.now().toString();
        garagePage.verifyCarAdded("Audi", "TT")
                .verifyMileage("20")
                .verifyCarImage("audi.png")
                .verifyDateInMileage(todayDate);
    }
}

