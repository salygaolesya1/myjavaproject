package pages4;

import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.*;

public class AddCarModal {
    private SelenideElement brandDropdown = $x("//select[@name='brand']");
    private SelenideElement modelInput = $x("//input[@name='model']");
    private SelenideElement mileageInput = $x("//input[@name='mileage']");
    private SelenideElement addButton = $x("//button[text()='Add']");

    public GaragePage addCar(String brand, String model, String mileage) {
        brandDropdown.selectOption(brand);
        modelInput.setValue(model);
        mileageInput.setValue(mileage);
        addButton.click();
        return new GaragePage();
    }
}

