package pages4;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static org.hamcrest.CoreMatchers.endsWith;

public class GaragePage {
    private SelenideElement addCarButton = $x("//button[text()='Add Car']");
    private SelenideElement carUpdateMileage = $x("//p[@class='car_update-mileage']");
    private SelenideElement carList = $x("//div[@class='car_list']"); // assuming there's a container for cars

    public AddCarModal openAddCarModal() {
        addCarButton.click();
        return new AddCarModal();
    }

    public GaragePage verifyCarAdded(String brand, String model) {
        carList.shouldHave(text(brand + " " + model));
        return this;
    }

    public GaragePage verifyMileage(String expectedMileage) {
        $x("//input[@name='miles']").shouldHave(value(expectedMileage));
        return this;
    }

    public GaragePage verifyCarImage(String expectedImageEnd) {
        SelenideElement carLogo = $x("//div[@class='car_logo car-logo']");
        carLogo.find("img").shouldHave(attribute("src", endsWith(expectedImageEnd)));
        return this;
    }

    public GaragePage verifyDateInMileage(String date) {
        carUpdateMileage.shouldHave(text(date));
        return this;
    }
}

