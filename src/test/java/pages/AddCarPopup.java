package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AddCarPopup {
    private WebDriver driver;
    private By brandSelect = By.xpath("//select[@id='addCarBrand']/option[text()='Audi']\n");
    private By modelSelect = By.xpath("//select[@id='addCarModel']/option[text()='TT']\n");
    private By mileageInput = By.xpath("//input[@id='addCarMileage']\n");
    private By addButton = By.xpath("//button[@class='btn btn-primary' and text()='Add']\n");

    public AddCarPopup(WebDriver driver) {
        this.driver = driver;
    }

    public void setCarDetails(String brand, String model, String mileage) {
        WebElement brandElement = driver.findElement(brandSelect);
        brandElement.sendKeys(brand);

        WebElement modelElement = driver.findElement(modelSelect);
        modelElement.sendKeys(model);

        WebElement mileageElement = driver.findElement(mileageInput);
        mileageElement.sendKeys(mileage);
    }

    public void clickAdd() {
        WebElement button = driver.findElement(addButton);
        button.click();
    }
}

