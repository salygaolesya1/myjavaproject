package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GaragePage {
    private WebDriver driver;

    private By addCarButton = By.xpath("//button[contains(@class, 'btn') and contains(@class, 'btn-primary')]\n");
    private By carEntries = By.id("//select[@id='addCarBrand']\n");
    private By mileageParagraph = By.id("//input[@id='addCarMileage']\n");
    private By milesInput = By.cssSelector("input#miles");
    private By carLogo =  By.id("//select[@id='addCarModel']\n");


    public GaragePage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickAddCar() {
        WebElement button = driver.findElement(addCarButton);
        button.click();
    }

    public boolean isCarDisplayed(String carName) {
        return driver.findElements(carEntries).stream()
                .anyMatch(entry -> entry.getText().contains(carName));
    }

    public String getMileageText() {
        WebElement paragraph = driver.findElement(mileageParagraph);
        return paragraph.getText();
    }


        public String getMilesValue() {
        WebElement input = driver.findElement(milesInput);
        return input.getAttribute("value");
    }

    public boolean isCarLogoDisplayed() {
        WebElement logo = driver.findElement(carLogo);
        return logo.isDisplayed();
    }

    public String getCarLogoSrc() {
        WebElement logo = driver.findElement(carLogo);
        return logo.getAttribute("src");
    }
}

