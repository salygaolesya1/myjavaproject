package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
    private WebDriver driver;
    private By guestLoginButton = By.xpath("//button[contains(@class, 'header-link') and contains(@class, '-guest')]\n");

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickGuestLogin() {
        WebElement button = driver.findElement(guestLoginButton);
        button.click();
    }
}

