import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class SocialMediaIconsTest {
    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        System.setProperty("chromedriver", "/Users/mac/IdeaProjects/MyJavaProject/src/main/resources/chromedriver");
        driver = new ChromeDriver();
    }

    @Test
    public void testSocialMediaIcons() {
        driver.get("https://guest:welcome2qauto@qauto.forstudy.space/");
        driver.manage().window().maximize();

        WebElement socialMediaBlock = driver.findElement(By.xpath("//div[@class='contacts_socials socials']"));

        List<WebElement> icons = socialMediaBlock.findElements(By.xpath(".//a[@class='socials_link']"));
        Assert.assertEquals(icons.size(), 5, "Social network block doesn’t contain 5 items");

        List<String> expectedUrls = List.of(
                "https://www.facebook.com/Hillel.IT.School",
                "https://t.me/ithillel_kyiv",
                "https://www.youtube.com/user/HillelITSchool?sub_confirmation=1",
                "https://www.instagram.com/hillel_itschool/",
                "https://www.linkedin.com/school/ithillel/"
        );

        for (WebElement icon : icons) {
            String originalHandle = driver.getWindowHandle();
            icon.click();

            List<String> handles = new ArrayList<>(driver.getWindowHandles());
            Assert.assertEquals(handles.size(), 2, "New tab did not open after clicking");

            driver.switchTo().window(handles.get(1));

            String currentUrl = driver.getCurrentUrl();
            boolean urlIsValid = expectedUrls.stream().anyMatch(currentUrl::equals);
            Assert.assertTrue(urlIsValid, "Incorrect url of Social network");

            driver.close();
            driver.switchTo().window(originalHandle);
        }
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
