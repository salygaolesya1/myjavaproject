import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static io.restassured.RestAssured.given;

public class CarsBrandsApiTest {

    @Test
    public void testCarsBrandsResponse() {
        RestAssured.baseURI = "https://qauto.forstudy.space";

        Response response = given()
                .when()
                .get("/api/cars/brands");

        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(response.getStatusCode(), 200, "Невірний код статусу.");

        boolean containsExpectedData = response.jsonPath().getList("findAll { it.id == 1 }").stream()
                .anyMatch(car -> ((Map<String, Object>) car).get("title").equals("Audi"));

        softAssert.assertTrue(containsExpectedData, "Тіло відповіді не містить об'єкт з id: 1 та title: Audi.");

        softAssert.assertAll();
    }
}
