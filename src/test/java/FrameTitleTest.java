import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class FrameTitleTest {
    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        System.setProperty("chromedriver", "/Users/mac/IdeaProjects/MyJavaProject/src/main/resources/chromedriver");
        driver = new ChromeDriver();
    }

    @Test
    public void testFrameTitle() {
        driver.get("https://guest:welcome2qauto@qauto.forstudy.space/");

        driver.switchTo().frame("iframe.hero-video_frame");

        String expectedTitle = "Hillel IT School | Учись ради мечты! - YouTube";
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, expectedTitle, "Title doesn’t equal to the expected result");
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}

